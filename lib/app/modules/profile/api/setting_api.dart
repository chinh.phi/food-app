import 'dart:convert';
import 'package:food_app/app/modules/address/providers/address_choose_provider.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class SettingApi {

  static var client = http.Client();
  static const _baseURL = "https://shopee-food-mobile.herokuapp.com";

  static getUserInfo() async {
    return await AddressChooseProvider.getUserInfo();
  }

  static updateInfo(String column, String value) async {

    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString("token");

    var response = await client.put(Uri.parse('$_baseURL/user/update-info'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer $token'
        },
        body: jsonEncode(<String, String>{"column": column, "updateValue": value}));
    if(response.statusCode == 200) {
      return response.body;
    } else {
      return "Lỗi";
    }
  }

}