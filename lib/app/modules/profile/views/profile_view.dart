import 'package:flutter/material.dart';
import 'package:food_app/app/modules/profile/views/address/address_view.dart';
import 'package:food_app/splash_screen.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../login/controllers/login_controller.dart';
import '../controllers/profile_controller.dart';
import 'payment/payment_view.dart';
import 'profile_menu.dart';
import 'profile_pic.dart';
import 'settings/setting_view.dart';



class ProfileView extends GetView<ProfileController> {
  const ProfileView({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {

    return SingleChildScrollView(
      // padding: EdgeInsets.symmetric(vertical: 20),
      child: Column(
        children: [
          ProfilePic(),
          ProfileMenu(
            text: "Cài đặt",
            icon: Icons.settings,
            press: () {
              Get.to(() => SettingView());
            },
          ),
          ProfileMenu(
            text: "Đăng xuất",
            icon: Icons.logout,
            press: () async{
              var loginController = Get.put(LoginController());
              bool a = await loginController.logout();
              SharedPreferences prefs = await SharedPreferences.getInstance();
              prefs.remove('token');
              prefs.remove('refreshToken');
              Get.offAll(() => SplashScreen());

            },
          ),

        ],
      ),
    );
  }
}

