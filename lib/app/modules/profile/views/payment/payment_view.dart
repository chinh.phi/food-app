import 'package:flutter/material.dart';
import 'package:food_app/app/modules/profile/views/payment/payment_item.dart';
import 'package:food_app/dimensions.dart';
import 'package:get/get.dart';

class PaymentView extends StatelessWidget {
  const PaymentView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Thanh toán', style: TextStyle(color: Colors.black)),
        elevation: 0,
        centerTitle: true,
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: Dimensions.primaryColor
          ),
          onPressed: () {
            Get.back();
          },
        ),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          padding: const EdgeInsets.only(top: 10),
          child: Column(
            children: [
              PaymentItem(
                text: 'Ví ShoppePay',
                press: () {

                },
              ),
              PaymentItem(
                text: 'Thẻ Tín dụng/ Ghi nợ',
                press: () {

                },
              ),
              PaymentItem(
                text: 'ShoppeFood Credits',
                press: () {

                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
