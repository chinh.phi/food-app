import 'dart:convert';

import 'package:food_app/app/modules/home/models/home_menu_model.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class HomeMenuApi {
  static var client = http.Client();
  static const _baseURL = "https://shopee-food-mobile.herokuapp.com";

  static List<HomeMenuModel> parses(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<HomeMenuModel>((json) =>HomeMenuModel.fromJson(json)).toList();
  }


  static Future<List<HomeMenuModel>> getMenuGrid() async {
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString("token");

    var response = await client.get(Uri.parse('$_baseURL/restaurant/category/popular?limit=11'),
        headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer $token'
        });

    if(response.statusCode == 200) {
      var json = response.body;
      return parses(json);
    } else {
      return [];
    }
  }
}