import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

import 'package:http/http.dart' as http;

class MerchantBranchListApi {

  static var client = http.Client();
  static const _baseURL = "https://shopee-food-mobile.herokuapp.com";

  static Future<List<dynamic>> getListBranch(List ids) async {
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString("token");
    var requestIds = ids.join(',');
    var response = await client.get(Uri.parse('$_baseURL/homepage/restaurants?filter=id&restaurantsId=$requestIds'),headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer $token'
    });
    if(response.statusCode == 200) {
      var json = response.body;
      return jsonDecode(json);
    } else {
      return [];
    }
  }
}