import 'package:flutter/material.dart';
import 'package:food_app/app/modules/address/views/address_choose_view.dart';
import 'package:food_app/app/modules/home/controllers/address_bar_controller.dart';
import 'package:get/get.dart';


class AddressBar extends GetView<AddressBarController> {
  const AddressBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.lazyPut(() => AddressBarController());
    return Row(
        mainAxisAlignment:
        MainAxisAlignment.spaceBetween,
        children: [
          const Icon(
            Icons.home,
            color: Color(0xFF2e3a59),
            size: 36.0,
          ),
          Expanded(
            child: Column(
                crossAxisAlignment:
                CrossAxisAlignment.start,
                children: [
                  const Text("Địa chỉ"),
                  InkWell(
                    child: Obx(() => Text(
                        controller.address.value,
                        overflow: TextOverflow.ellipsis)),
                    onTap: () {
                      Get.to(() => AddressChooseView());
                    },
                  )

                ]),
          ),
          const SizedBox(
            width: 52,
            height: 52,
            child: Icon(Icons.favorite,
                color: Color(0xFF97d5c8), size: 36.0),
          )
        ]);
  }
}
