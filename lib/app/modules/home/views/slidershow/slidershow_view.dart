import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:food_app/dimensions.dart';
import 'package:food_app/failure_view.dart';
import 'package:get/get.dart';

import '../../controllers/slidershow_controller.dart';



class SliderShowView extends GetView<SliderShowController> {
  @override
  Widget build(BuildContext context) {
    Get.lazyPut(() => SliderShowController());
    return Obx(() {
      if (controller.isDataProcessing.value) {
        return Center(
          child: Container(
            margin: const EdgeInsets.all(8),
            child: const CircularProgressIndicator(backgroundColor: Dimensions.primaryColor,),
          ),
        );
      } else {
        if (controller.isDataError.value) {
          return FailureView(onPressed: () => controller.getBanner());
        } else {
          return Container(
            color: Colors.white,
            child: Column(
              children: [
                CarouselSlider(
                  items: generateSlider(),
                  options: CarouselOptions(
                    height: 200,
                    autoPlay: true,
                    enlargeCenterPage: true,
                    enlargeStrategy: CenterPageEnlargeStrategy.height,
                    onPageChanged: (index, reason) {
                      controller.currentIndex.value = index;
                    }
                  ),
                ),
                // Row(
                //   mainAxisAlignment: MainAxisAlignment.center,
                //   children: controller.listBanner.map((urlOfItem) {
                //     int index = controller.listBanner.indexOf(urlOfItem);
                //     return Container(
                //       width: 10.0,
                //       height: 10.0,
                //       margin: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                //       decoration: BoxDecoration(
                //         shape: BoxShape.circle,
                //         color: controller.currentIndex.value == index
                //             ? const Color.fromRGBO(0, 0, 0, 0.8)
                //             : const Color.fromRGBO(0, 0, 0, 0.3),
                //       ),
                //     );
                //   }).toList(),
                // )
              ],
            ),
          );
        }
      }
    });
  }

  List<Widget> generateSlider() {
    List<Widget> imageSlider = controller.listBanner
        .map((item) => Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                margin: const EdgeInsets.only(
                  top: 10.0,
                  bottom: 10.0,
                ),
                elevation: 6.0,
                shadowColor: Colors.redAccent,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: ClipRRect(
                  borderRadius: const BorderRadius.all(
                    Radius.circular(10.0),
                  ),
                  child: Stack(
                    children: <Widget>[
                      Image.network(
                        item['image_thumbnail_path'],
                        fit: BoxFit.cover,
                        width: double.infinity,
                        errorBuilder: (context, url, error) => const Icon(Icons.error, color: Colors.red),
                      )
                    ],
                  ),
                ),
              ),
            ))
        .toList();
    return imageSlider;
  }
}
