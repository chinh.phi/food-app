import 'package:flutter/material.dart';
import 'package:food_app/app/modules/home/controllers/home_menu_controller.dart';
import 'package:food_app/app/modules/home/views/menu/menu_grid.dart';
import 'package:food_app/dimensions.dart';
import 'package:get/get.dart';

class HomeMenuView extends GetView<HomeMenuController> {
  const HomeMenuView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.lazyPut(() => HomeMenuController());
    return Obx(() =>
        controller.isDataProcessing.value
            ? const SliverToBoxAdapter(child: Center(child: CircularProgressIndicator(backgroundColor: Dimensions.primaryColor,)))
            : SliverPadding(padding: EdgeInsets.symmetric(horizontal: 10), sliver: MenuGrid(lists: controller.listMenu))
    );
  }
}
