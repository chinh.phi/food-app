import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CategoryView extends StatelessWidget {
  final List<CardItem> items = [
    const CardItem(
      urlImage: 'assets/images/1.png',title: 'Áp dụng từ 02 voucher mỗi đơn',
    ),
    const CardItem(
      urlImage: 'assets/images/2.png',title: 'Áp dụng từ 02 voucher mỗi đơn',
    ),
    const CardItem(
      urlImage: 'assets/images/3.png',title: 'Áp dụng từ 02 voucher mỗi đơn',
    ),
    const CardItem(
      urlImage: 'assets/images/4.png',title: 'Áp dụng từ 02 voucher mỗi đơn',
    ),
    const CardItem(
      urlImage: 'assets/images/5.png',title: 'Áp dụng từ 02 voucher mỗi đơn',
    ),
    const CardItem(
      urlImage: 'assets/images/6.png',title: 'Áp dụng từ 02 voucher mỗi đơn',
    ),
    const CardItem(
      urlImage: 'assets/images/6.png',title: 'Áp dụng từ 02 voucher mỗi đơn',
    ),
    const CardItem(
      urlImage: 'assets/images/6.png',title: 'Áp dụng từ 02 voucher mỗi đơn',
    ),
    const CardItem(
      urlImage: 'assets/images/6.png',title: 'Áp dụng từ 02 voucher mỗi đơn',
    ),
    const CardItem(
      urlImage: 'assets/images/6.png',title: 'Áp dụng từ 02 voucher mỗi đơn',
    ),
    const CardItem(
      urlImage: 'assets/images/6.png',title: 'Áp dụng từ 02 voucher mỗi đơn',
    ),

  ];
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150,
        width: double.infinity,
        decoration: const BoxDecoration(
          color: Colors.white,
        ),
        child: ListView.separated(
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) => buidCard(items[index]),
          separatorBuilder: (context, _) => const SizedBox(width: 10),
          itemCount: items.length,
        )
    );
  }

  Widget buidCard(CardItem item) => GestureDetector(
    onTap: () {
      //choose category
      print('Choose');
    },
    child: Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10)
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
              height: 100,
              width: 220,
              child: ClipRRect(
                child: Image.asset(item!.urlImage!, fit: BoxFit.cover),
                borderRadius: BorderRadius.circular(10),
              )
          ),
          const SizedBox(height: 4),
          Text(item!.title!, style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w500)),
        ],
      ),
    ),
  );
}

class CardItem {
  final String? urlImage;
  final String? title;

  const CardItem({
    this.urlImage,
    this.title
  });
}