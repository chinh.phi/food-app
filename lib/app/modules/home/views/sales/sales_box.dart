import 'package:flutter/material.dart';
import 'package:food_app/app/modules/home/views/sales/category_view.dart';

class SalesBox extends StatelessWidget {
  const SalesBox({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 10),
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(color: Color(0xFFe8e8e8))
        )
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Khuyến mãi đa tầng',
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500)
          ),
          SizedBox(height: 10),
          CategoryView()
        ],
      )
    );
  }
}
