import 'package:flutter/material.dart';
import 'package:food_app/app/modules/home/controllers/suggestion_controller.dart';
import 'package:food_app/app/modules/home/models/suggestion_model.dart';
import 'package:food_app/app/modules/home/views/suggestion/suggestion_box.dart';
import 'package:food_app/dimensions.dart';
import 'package:get/get.dart';

class SuggestionView extends GetView<SuggestionController> {
  const SuggestionView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.lazyPut(() => SuggestionController());
    return Obx(() =>
        controller.isDataProcessing.value
            ? const SliverToBoxAdapter(child: Center(child: CircularProgressIndicator(backgroundColor: Dimensions.primaryColor,)))
            : SliverList(
          delegate: SliverChildBuilderDelegate(
                (BuildContext context, int index) {
              var data = SuggestionModel.fromJson(controller.listData[index]);
              return SuggestionBox(
                item: data,
              );
            },
            childCount: controller.listData.length, // 1000 list items
          ),
        )
    );
  }
}
