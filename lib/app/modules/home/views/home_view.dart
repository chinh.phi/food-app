
import 'package:flutter/material.dart';
import 'package:food_app/app/modules/home/views/address/address_bar.dart';
import 'package:food_app/app/modules/home/views/menu/home_menu_view.dart';
import 'package:food_app/app/modules/home/views/search/search_bar.dart';
import 'package:food_app/app/modules/home/views/slidershow/slidershow_view.dart';
import 'package:get/get.dart';

import 'suggestion/suggestion_view.dart';
class HomeView extends StatelessWidget {
  const HomeView({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Color(0xFFe8e8e8),
      body: Stack(
        children: [
          _buildContent(),
          _buildTopBar()
        ],
      ),
    );
  }

  _buildContent() {
    return CustomScrollView(
      slivers: [
        _buildSliderShow(),
        _buildMenu(),
        // _buildSales(),
        const SliverPadding(
            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
            sliver: SliverToBoxAdapter(
                child: Text('Quanh đây có gì ngon?', style: TextStyle(fontSize:20, fontWeight: FontWeight.bold))
            )
        ),
        _buildSuggestion()
      ],
    );
  }

  _buildSliderShow() {
    return SliverToBoxAdapter(
        child: Container(
            margin: const EdgeInsets.only(top: 150),
            child: SliderShowView()
        )
    );
  }

  _buildMenu() {
    return HomeMenuView();
  }

  // _buildSales() {
  //   return const SliverPadding(
  //       padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 10),
  //       sliver: SliverToBoxAdapter(
  //           child: SalesBox()
  //       )
  //   );
  // }

  _buildSuggestion() {
    return SuggestionView();
  }

  _buildTopBar() {
    return Positioned(
        top: 0,
        left: 0,
        right: 0,
        child: Container(
          padding: EdgeInsets.only(top: 40),
            decoration: const BoxDecoration(
              color: Colors.white,
            ),
            child: Column(
              children: const [
                AddressBar(),
                SearchBar()
              ],
            ))
    );
  }
}