import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class MerchantDetailApi {
  static var client = http.Client();
  static const _baseURL = "https://shopee-food-mobile.herokuapp.com";
  static Future<dynamic> getListBranch(id) async {
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString("token");

    var response = await client.get(Uri.parse('$_baseURL/restaurant/$id/details'),headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer $token'
    });
    if(response.statusCode == 200) {
      var json = response.body;
      // var res = SuggestionModel.fromJson(jsonDecode(response.body));
      return jsonDecode(json);
    } else {
      return "lỗi";
    }
  }
}