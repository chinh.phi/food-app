import 'package:flutter/material.dart';
import 'package:food_app/app/modules/merchant/api/merchant_detail_api.dart';
import 'package:food_app/app/modules/merchant/models/merchant_detail_model.dart';
import 'package:get/get.dart';

import '../models/food_item_model.dart';
import '../models/menu_food_model.dart';
import '../models/partner_info_model.dart';

class MerchantController extends GetxController with GetSingleTickerProviderStateMixin {

  var merchantDetailInfo = MerchantDetailModel().obs;
  var id = Get.parameters["id"]!.substring(1);
  var isDataProcessing = false.obs;

  var menu = [].obs;

  List<MenuFoodModel> listFoods = [
    MenuFoodModel(
        name: 'Nhât định phải thử',
        lists: [
          Food(name: 'Sữa chua Unicorn', price: 35000, imageLink: 'assets/images/sua_chua_tran_chau.png'),
          Food(name: 'Sữa chua Dứa', price: 35000, imageLink: 'assets/images/sua_chua_tran_chau.png'),
        ]
    ),
    MenuFoodModel(
        name: 'Món mới',
        lists: [
          Food(name: 'Sữa chua Chocomint', price: 35000, imageLink: 'assets/images/sua_chua_tran_chau.png'),
          Food(name: 'Sữa chua Summer', price: 35000, imageLink: 'assets/images/sua_chua_tran_chau.png'),
          Food(name: 'Sữa chua Mintblue Sea', price: 35000, imageLink: 'assets/images/sua_chua_tran_chau.png'),
          Food(name: 'Sữa chua Sunrise', price: 35000, imageLink: 'assets/images/sua_chua_tran_chau.png'),
        ]
    ),

  ];

  PartnerInfoModel merchantInfo = PartnerInfoModel(
      address: '43 Hồ Tùng Mậu, Phường Mai Dịch, Quận Cầu Giấy, Thành Phố Hà Nội',
      list: [
        openTime(date: 'Chủ Nhật', timeOpen: '09:00AM', timeClose: '09:00PM'),
        openTime(date: 'Thứ Hai', timeOpen: '09:00AM', timeClose: '09:00PM'),
        openTime(date: 'Thứ Ba', timeOpen: '09:00AM', timeClose: '09:00PM'),
        openTime(date: 'Thứ Tư', timeOpen: '09:00AM', timeClose: '09:00PM'),
        openTime(date: 'Thứ Năm', timeOpen: '09:00AM', timeClose: '09:00PM'),
        openTime(date: 'Thứ Sáu ', timeOpen: '09:00AM', timeClose: '09:00PM'),
        openTime(date: 'Thứ Bảy', timeOpen: '09:00AM', timeClose: '09:00PM'),
      ]
  );
  var numOfFood = 0.obs;
  var money = ''.obs;

  List<double> offsetItems = [];
  List<double> offsetMenus = [];
  List<double> itemHeightLists = [];
  List<GlobalKey> itemKeyLists = [];

  List<double> menuWidthLists = [];
  List<GlobalKey> menuKeyLists = [];

  late ScrollController scrollController;
  late ScrollController menuScrollController;
  var currentIndex = 0.obs;
  var isScrolling = false.obs;
  var offsetScrollController = 0.0.obs;
  var isFavorite = false.obs;



  @override
  void onInit() async {
    super.onInit();
    scrollController = ScrollController();
    menuScrollController = ScrollController();
    await getData();
    offsetItems = List.generate(menu.length, (index) => index.toDouble());
    // offsetMenus = List.generate(menu.length, (index) => index.toDouble());
    itemHeightLists = List.generate(menu.length, (index) => index.toDouble());
    menuWidthLists = List.generate(menu.length, (index) => index.toDouble());
    itemKeyLists = List.generate(menu.length, (index) => GlobalKey());
    menuKeyLists = List.generate(menu.length, (index) => GlobalKey());
  }

  onStartScroll(ScrollMetrics metrics) {
    return '';
  }

  onUpdateScroll(ScrollMetrics metrics) {
    isScrolling.value = true;
    offsetScrollController.value = scrollController.offset;
    for(var i = 0; i < offsetItems.length; i++) {
      if(offsetScrollController.value + itemHeightLists[i]/2 >= offsetItems[i]) {
        currentIndex.value = i;
        scrollMenu(currentIndex.value);

      }
    }
    return '';
  }

  onEndScroll(ScrollMetrics metrics) {
    isScrolling.value = false;
    return '';
  }

  clickToMenu(index) {
    currentIndex.value = index;
    scrollController.animateTo(offsetItems[index],
        duration: Duration(milliseconds: 100), curve: Curves.easeIn);
    scrollMenu(index);

  }

  scrollMenu(index) {
    menuScrollController.animateTo(offsetMenus[index],
        duration: Duration(milliseconds: 100), curve: Curves.easeIn);
  }

  clickFavorite() {
    isFavorite.value = !isFavorite.value;
  }

  callback(result) {
    money.value = result[0]["price"];
    numOfFood.value = result[1]["number"];
    print(money.value);
    print(numOfFood.value);
  }

  @override
  void onClose() {
    super.onClose();
    scrollController.dispose();
  }

  getData() async {
    try {
      isDataProcessing(true);
      print(id);
      var res = await MerchantDetailApi.getListBranch(id);
      merchantDetailInfo.value = MerchantDetailModel.fromJson(res);
      getMenu();
      isDataProcessing(false);
    } catch (exception) {
      // isDataProcessing(false);
      // showSnackBar("Exception", exception.toString(), Colors.red);
    }
  }

  void getMenu() {
    var listItem = merchantDetailInfo.value.food;
    var list = listItem!.map((value) => value.name!.split(" ")[0]);
    menu.value = list.toSet().toList();
    // for (var value in menu) {
    //   for(var item in listItem) {
    //     // if()
    //   }
    // }
  }

  void getItemOfMenu(menu) {
    
  }

}