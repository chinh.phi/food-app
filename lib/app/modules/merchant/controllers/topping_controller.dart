import 'package:get/get.dart';

import '../models/topping_model.dart';

class ToppingController extends GetxController {

  final List<ToppingModel> itemLists = [
    ToppingModel(
      name: 'TOPPING SỮA CHUA',
      maxTopping: 4,
      toppingLists: [
        ToppingItemModel(name: 'Trân Châu', price: '5000'),
        ToppingItemModel(name: 'Dừa Khô', price: '5000'),
        ToppingItemModel(name: 'Chuối Khô', price: '5000'),
        ToppingItemModel(name: 'Nho Khô', price: '10000'),
        ToppingItemModel(name: 'Nha Đam', price: '10000'),
        ToppingItemModel(name: 'Đậu Đỏ', price: '5000'),
        ToppingItemModel(name: 'Nếp Cẩm', price: '5000'),
        ToppingItemModel(name: 'Thạch Dừa', price: '5000'),
        ToppingItemModel(name: 'Trân Châu Đường Đen', price: '10000'),
      ]
    ),
    ToppingModel(
      name: 'KÍCH THƯỚC',
      maxTopping: 1,
      toppingLists: [
        ToppingItemModel(name: 'SIZE M', price: '0'),
        ToppingItemModel(name: 'SIZE L', price: '7000')
      ]
    )
  ];

  var numberOfFood = 1.obs;
  var moneyInit = Get.arguments[0]['price'];
  var moneyTopping = '0'.obs;
  var money = '0'.obs;

  var isAdd = false.obs;

  @override
  void onInit() {
    super.onInit();
    money.value = moneyInit;
  }

  clickRemoveFood() {
    if(numberOfFood > 1) {
      numberOfFood--;
      money.value = ((int.parse(moneyInit) + int.parse(moneyTopping.value))*numberOfFood.value).toString();
    }
  }

  clickAddFood() {
    numberOfFood++;
    money.value = ((int.parse(moneyInit) + int.parse(moneyTopping.value))*numberOfFood.value).toString();
  }


  callback(toppingMoney) {
    moneyTopping.value = toppingMoney;
    money.value = ((int.parse(moneyInit) + int.parse(moneyTopping.value))*numberOfFood.value).toString();
  }

  clickAddButton() {
    Get.back(result: [
      {"price": money.value},
      {'number': numberOfFood.value}
    ]);
    isAdd.value = true;
  }

  closeView() {
    Get.back(result: [
      {"price": '0'},
      {'number': 0}
    ]);
  }

}