import 'package:get/get.dart';

import '../models/cart_item_model.dart';

class CartController extends GetxController {
  List<CartItemModel> lists = [
    CartItemModel(name: 'Trà sữa nướng trân châu bách nghệ', detail: 'Cỡ M, 70% đá, 70% đường, Trân châu đen, Trân châu trắng, Trân châu sợi', oldPrice: '52000', newPrice: '41600', number: 1),
    CartItemModel(name: 'Trà sữa nướng trân châu bách nghệ', detail: 'Cỡ M, 70% đá, 70% đường, Trân châu đen, Trân châu trắng', oldPrice: '52000', newPrice: '41600', number: 1),
    CartItemModel(name: 'Trà sữa nướng trân châu bách nghệ', detail: 'Cỡ M, 70% đá, 70% đường, Trân châu đen, Trân châu trắng', oldPrice: '52000', newPrice: '41600', number: 1),
    CartItemModel(name: 'Trà sữa nướng trân châu bách nghệ', detail: 'Cỡ M, 70% đá, 70% đường, Trân châu đen, Trân châu trắng', oldPrice: '52000', newPrice: '41600', number: 1),
    CartItemModel(name: 'Trà sữa nướng trân châu bách nghệ', detail: 'Cỡ M, 70% đá, 70% đường, Trân châu đen, Trân châu trắng', oldPrice: '52000', newPrice: '41600', number: 1),
  ];

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
  }

  @override
  void onClose() {
    // TODO: implement onClose
    super.onClose();
  }
}