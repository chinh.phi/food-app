class CartItemModel {
  String? name;
  String? detail;
  String? oldPrice;
  String? newPrice;
  int? number;

  CartItemModel({this.name, this.detail, this.oldPrice, this.newPrice, this.number});
}