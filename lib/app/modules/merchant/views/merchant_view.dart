import 'package:flutter/material.dart';
import 'package:food_app/app/modules/merchant/views/cart_view.dart';
import 'package:food_app/app/modules/merchant/views/merchant_comment.dart';
import 'package:food_app/app/modules/merchant/views/merchant_info.dart';
import 'package:food_app/app/modules/merchant/views/widgets/get_box_offset.dart';
import 'package:food_app/app/modules/merchant/views/widgets/menu_item_order.dart';
import 'package:food_app/app/modules/profile/views/payment/payment_view.dart';
import 'package:food_app/dimensions.dart';
import 'package:food_app/re_use_function.dart';
import 'package:get/get.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

import '../controllers/merchant_controller.dart';

class MerchantView extends GetView<MerchantController> {
  const MerchantView({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    Get.lazyPut<MerchantController>(() => MerchantController());
    print(MediaQuery.of(context).size.height);
    return Obx(() => Scaffold(
      body: controller.isDataProcessing.value
          ? Center(child: CircularProgressIndicator())
          : NotificationListener<ScrollNotification>(
          onNotification: (scrollNotification) {
            if (scrollNotification is ScrollStartNotification) {
              controller.onStartScroll(scrollNotification.metrics);
            } else if (scrollNotification is ScrollUpdateNotification) {
              controller.onUpdateScroll(scrollNotification.metrics);
            } else if (scrollNotification is ScrollEndNotification) {
              controller.onEndScroll(scrollNotification.metrics);
            }
            return true;
          },
          child: Stack(
              children: [
                SingleChildScrollView(
                    controller: controller.scrollController,
                    child: Column(
                        children: [
                          Container(
                            height: MediaQuery.of(context).size.height*0.4,
                            child: Stack(
                              children: [
                                Container(
                                    height: MediaQuery.of(context).size.height*0.3,
                                    child: Stack(
                                      children: [
                                        Container(
                                          decoration: BoxDecoration(
                                              color: Colors.transparent,
                                              image: DecorationImage(
                                                  image: AssetImage(
                                                      'assets/images/sua_chua_tran_chau.png'
                                                  ),
                                                  fit: BoxFit.cover
                                              )
                                          ),
                                        ),
                                        Container(
                                          decoration: BoxDecoration(
                                            gradient: LinearGradient(
                                                begin: FractionalOffset.bottomCenter,
                                                end: FractionalOffset.topCenter,
                                                colors: [
                                                  Colors.grey.withOpacity(0.0),
                                                  const Color(0xFF424242).withOpacity(0.4),
                                                ],
                                                stops: const [
                                                  0.0,
                                                  1.0
                                                ]
                                            ),

                                          ),
                                        )
                                      ],
                                    )
                                ),
                                Align(
                                    alignment: Alignment.bottomCenter,
                                    child: InkWell(
                                      child: Container(
                                          height: MediaQuery.of(context).size.height*0.2,
                                          margin: const EdgeInsets.symmetric(horizontal: 30),
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius: BorderRadius.circular(10),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Colors.grey.withOpacity(0.4),
                                                  blurRadius: 5.0, // soften the shadow
                                                  spreadRadius: 2.0, //extend the shadow
                                                  offset: const Offset(
                                                    1.0,
                                                    1.0,
                                                  ),
                                                )
                                              ]
                                          ),
                                          child: Padding(
                                            padding: const EdgeInsets.symmetric(horizontal: 16),
                                            child: Column(
                                              children: [
                                                const SizedBox(height: 10),
                                                Row(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  children: const [
                                                    Icon(
                                                        Icons.verified_rounded,
                                                        color: Dimensions.primaryColor
                                                    ),
                                                    SizedBox(width: 5),
                                                    Text('ĐÔÍ TÁC CỦA 247', style: TextStyle(color: Dimensions.primaryColor, fontSize: 18))
                                                  ],
                                                ),
                                                const SizedBox(height: 10),
                                                Row(
                                                  children: [
                                                    Flexible(
                                                        child: Text(
                                                          'SỮA CHUA TRÂN CHÂU HẠ LONG - HỒ TÙNG MẬU',
                                                          style: TextStyle(
                                                              color: Colors.black,
                                                              fontSize: 22,
                                                              fontWeight: FontWeight.bold),
                                                          textAlign: TextAlign.center,
                                                        )
                                                    )
                                                  ],
                                                ),
                                                const SizedBox(
                                                    height: 20
                                                ),
                                                Text(
                                                  '0.5km | 45 Hồ Tùng Mậu, Phường Mai Dịch, Quận Cầu Giấy, Thành Phố Hà Nội',
                                                  style: TextStyle(
                                                    fontSize: 16,
                                                  ),
                                                  overflow: TextOverflow.ellipsis,
                                                )
                                              ],
                                            ),
                                          )
                                      ),
                                      onTap: () {
                                        showCupertinoModalBottomSheet(
                                            context: context,
                                            builder: (context) => PartnerInfo(item: controller.merchantInfo),
                                            expand: true,
                                            isDismissible: false,
                                            duration: Duration(milliseconds: 300)
                                        );
                                      },
                                    )
                                )
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 16),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Row(
                                    children: [
                                      Icon(
                                        Icons.star_rate_rounded,
                                        color: Colors.yellow,
                                        size: 30,
                                      ),
                                      SizedBox(width: 5),
                                      Text('4.1', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16)),
                                      SizedBox(width: 5),
                                      Text('(100+)', style: TextStyle(color: Colors.black87, fontSize: 16)),
                                      SizedBox(width: 10),
                                      Icon(
                                        Icons.shopping_basket_outlined,
                                        color: Colors.black87,
                                        size: 26,
                                      ),
                                      SizedBox(width:5),
                                      Text('500+ đã bán', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
                                    ],
                                  ),
                                ),
                                InkWell(
                                  child: Text('Xem đánh giá', style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500, color: Dimensions.primaryColor)),
                                  onTap: () {
                                    Get.to(() => PartnerComment());
                                  },
                                )
                              ],
                            ),
                          ),
                          for(var index = 0 ; index < controller.offsetItems.length; index++)
                            GetBoxOffset(
                              key: controller.itemKeyLists[index],
                              offset: (offset) {
                                controller.offsetItems[index] = offset.dy-142;
                                controller.itemHeightLists[index] = controller.itemKeyLists[index].currentContext!.size!.height;
                              },
                              child: MenuItemOrder(
                                menuLists: controller.listFoods[index], callbackFunction: controller.callback,
                              ),
                            )

                        ]
                    )
                ),
                Positioned(
                  top: 0,
                  left: 0,
                  right: 0,
                  child: Container(
                    // margin: EdgeInsets.only(top: 20),
                    padding: const EdgeInsets.only(top: 40, bottom: 20),
                    decoration: BoxDecoration(
                        color: controller.offsetScrollController.value != 0.0 ? Colors.white : Colors.transparent,
                        border: controller.offsetScrollController.value != 0.0
                            ? const Border(
                            bottom: BorderSide(
                                color: Color(0xFFe8e8e8)
                            )
                        )
                            : const Border()
                    ),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            IconButton(
                              padding: EdgeInsets.zero,
                              icon: Icon(Icons.arrow_back, color: controller.offsetScrollController.value != 0.0 ? Colors.black : Colors.white, size: 25,),
                              onPressed: () {
                                Get.back();
                              },
                            ),
                            Expanded(
                                child: controller.offsetScrollController.value != 0.0
                                    ? Container(
                                  child: TextField(
                                    // controller: _controller,
                                    onTap: () async {
                                    },
                                    decoration: InputDecoration(
                                      prefixIcon: const Icon(
                                        Icons.search,
                                        color: Color(0xFF3b3b3b),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                          borderRadius:
                                          BorderRadius.circular(30),
                                          borderSide: BorderSide(
                                              color: Colors.white)),
                                      focusedBorder: OutlineInputBorder(
                                          borderRadius:
                                          BorderRadius.circular(30),
                                          borderSide: BorderSide(
                                              color: Colors.white)),
                                      hintText: 'Tìm kiếm ở đây',
                                      contentPadding: EdgeInsets.all(0),
                                      fillColor: Color(0xFFF3F3F3),
                                      filled: true,
                                    ),
                                  ),
                                )
                                    : Container()
                            ),
                            IconButton(
                              padding: EdgeInsets.zero,
                              icon: Icon(
                                controller.isFavorite.value ? Icons.favorite : Icons.favorite_border,
                                color: controller.isFavorite.value
                                    ? Colors.red
                                    : controller.offsetScrollController.value == 0.0 ? Colors.white : Colors.black,
                                size: 25,
                              ),
                              onPressed: () {
                                controller.clickFavorite();
                              },
                            ),
                            controller.offsetScrollController.value == 0.0
                                ? IconButton(
                              padding: EdgeInsets.zero,
                              icon: Icon(
                                Icons.search,
                                color: controller.offsetScrollController.value != 0.0
                                    ? Colors.black
                                    : Colors.white,
                                size: 25,
                              ),
                              onPressed: () {},
                            )
                                : Container(),
                          ],
                        ),
                        controller.offsetScrollController.value != 0.0
                            ? Container(
                            margin: EdgeInsets.only(top: 10, left: 10, right: 10),
                            height: 40,
                            width: double.infinity,
                            child: NotificationListener(
                              child: ListView.separated(
                                controller: controller.menuScrollController,
                                scrollDirection: Axis.horizontal,
                                itemBuilder: (context, index) => GetBoxOffset(
                                    key: controller.menuKeyLists[index],
                                    offset: (offset) {
                                      controller.offsetMenus.add(offset.dx - 10);
                                      // controller.menuWidthLists[index] = controller.menuKeyLists[index].currentContext.size.width;
                                    },
                                    child: InkWell(
                                      onTap: () {
                                        controller.clickToMenu(index);
                                      },
                                      child: Container(
                                        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 6),
                                        decoration: BoxDecoration(
                                          color: controller.currentIndex == index ? Dimensions.primaryColor.withOpacity(0.3) : Colors.white,
                                          borderRadius: BorderRadius.circular(20),
                                        ),
                                        child: Center(child: Text("${controller.menu[index]}", style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500, color: Colors.black87))),
                                      ),
                                    )
                                ),
                                separatorBuilder: (context, _) => const SizedBox(width: 10),
                                itemCount: controller.menu.length,
                              ),
                              onNotification: (t) {
                                return true;
                                // if (t is ScrollEndNotification) {
                                //   return true;
                                //   // print(controller.menuScrollController.position.pixels);
                                // }
                              },
                            )
                        )
                            : Container()
                      ],
                    ),
                  ),
                ),
                controller.numOfFood.value != 0 ? Positioned(
                    left: 0,
                    right: 0,
                    bottom: 0,
                    child: Container(
                      padding: EdgeInsets.only(bottom: 20, left: 10, right: 10, top: 10),
                      // height: 60,
                      color: Colors.white,
                      child: Row(
                        children: [
                          Container(
                            width: 80,
                            height: 50,
                            margin: const EdgeInsets.only(left: 10),
                            decoration: BoxDecoration(
                                border: Border.all(color: Dimensions.primaryColor),
                                borderRadius: BorderRadius.circular(10)
                            ),
                            child: InkWell(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  const Icon(
                                    Icons.shopping_basket,
                                    color: Dimensions.primaryColor,
                                    size: 40,
                                  ),
                                  const SizedBox(width: 5,),
                                  Text('${controller.numOfFood.value}', style: const TextStyle(color: Dimensions.primaryColor, fontSize: 16))
                                ],
                              ),
                              onTap: () {
                                showCupertinoModalBottomSheet(
                                    context: context,
                                    builder: (context) => CartView(),
                                    expand: true,
                                    isDismissible: false,
                                    duration: const Duration(milliseconds: 300)
                                );
                              },
                            ),
                          ),
                          const SizedBox(width: 10),
                          Expanded(
                            child: InkWell(
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Dimensions.primaryColor,
                                ),
                                height: 50,
                                padding: const EdgeInsets.symmetric(horizontal: 20),
                                child: Center(child: InkWell(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      const Text('Trang thanh toán', style: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.w500)),
                                      SizedBox(width: 5,),
                                      const Icon(
                                          Icons.fiber_manual_record,
                                          size: 6,
                                          color: Colors.white
                                      ),
                                      SizedBox(width: 5,),
                                      Text('${format(int.parse(controller.money.value))}', style: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.w500))
                                    ],
                                  ),
                                  onTap: () {
                                    Get.to(() => PaymentView());
                                  },
                                )),
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                ) : Container()
              ]
          )
      ),
    ));
  }
}
