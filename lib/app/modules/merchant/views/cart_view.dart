import 'package:flutter/material.dart';
import 'package:food_app/dimensions.dart';
import 'package:food_app/re_use_function.dart';
import 'package:get/get.dart';

import '../controllers/cart_controller.dart';

class CartView extends GetView<CartController> {
  const CartView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.lazyPut(() => CartController());
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            title:
                const Text('Giỏ hàng', style: TextStyle(color: Colors.black, fontWeight: FontWeight.w500)),
            centerTitle: true,
            backgroundColor: Colors.white,
            elevation: 0,
            pinned: true,
            leading: IconButton(
              icon: const Icon(Icons.close_rounded, color: Colors.black),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            actions: [
              Center(
                  child: InkWell(
                      child: Container(
                        padding: const EdgeInsets.only(right: 10),
                          child: const Text('Xóa', style: TextStyle(color: Colors.red, fontSize: 18))
                      ),
                    onTap: () {

                    },
                  ),
              )
            ],
            bottom: PreferredSize(
                child: Container(
                  color: Color(0xFFe8e8e8),
                  height: 1.0,
                ),
                preferredSize: Size.fromHeight(1.0)
            ),
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate(
                  (BuildContext context, int index) {
                return Container(
                  padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  decoration: const BoxDecoration(
                      color: Colors.white,
                      border: Border(
                          bottom: BorderSide(
                              color: Color(0xFFe8e8e8)
                          )
                      )
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(controller.lists[index].name!, style: const TextStyle(fontSize: 18)),
                      const SizedBox(height: 5,),
                      Text(controller.lists[index].detail!, overflow: TextOverflow.ellipsis,),
                      const SizedBox(height: 20),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Text(format(int.parse(controller.lists[index].oldPrice!)), style: TextStyle(decoration: TextDecoration.lineThrough, color: Colors.black.withOpacity(0.6))),
                              const SizedBox(width: 5),
                              Text(format(int.parse(controller.lists[index].newPrice!)), style: const TextStyle(fontWeight: FontWeight.w500, fontSize: 15)),
                            ],
                          ),
                          Row(
                            children: [
                              Container(
                                width: 30,
                                height: 30,
                                decoration: BoxDecoration(
                                  color: Colors.grey.withOpacity(0.3),
                                  borderRadius: BorderRadius.circular(10)
                                  // border: Border.all(color: Colors.deepOrange),
                                ),
                                child: IconButton(
                                  padding: EdgeInsets.zero,
                                  icon: const Icon(Icons.remove_rounded, color: Dimensions.primaryColor,),
                                  onPressed: () {
                                    // _clickRemove();

                                  },
                                ),
                              ),
                              const SizedBox(width: 10),
                              Text('${controller.lists[index].number}', style: const TextStyle(fontSize: 18, fontWeight: FontWeight.w700)),
                              const SizedBox(width: 10),
                              Container(
                                width: 30,
                                height: 30,
                                decoration: BoxDecoration(
                                    color: Colors.grey.withOpacity(0.3),
                                    borderRadius: BorderRadius.circular(10)
                                  // border: Border.all(color: Colors.deepOrange),
                                ),
                                child: IconButton(
                                  padding: EdgeInsets.zero,
                                  icon: const Icon(Icons.add_rounded, color: Dimensions.primaryColor,),
                                  onPressed: () {
                                    // _clickRemove();

                                  },
                                ),
                              ),
                            ]
                          )
                        ],
                      )


                    ],
                  ),
                );
              },
              childCount: controller.lists.length, // 1000 list items
            ),
          ),
        ],
      ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.all(20.0),
        child: SizedBox(
          height: 50,
          child: ElevatedButton(
            onPressed: () {
              // controller.clickAddButton();
            },
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all<Color>(Dimensions.primaryColor),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('2 Món', style: TextStyle(fontSize: 18),),
                Text('Trang thanh toán', style: TextStyle(fontSize: 18)),
                Text('108.000đ', style: TextStyle(fontSize: 18))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
