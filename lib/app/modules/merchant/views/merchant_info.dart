import 'package:flutter/material.dart';

import '../models/partner_info_model.dart';

class PartnerInfo extends StatelessWidget {
  const PartnerInfo({Key? key, this.item}) : super(key: key);
  final PartnerInfoModel? item;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            title: const Text('Thông tin thêm', style: TextStyle(color: Colors.black)),
            backgroundColor: Colors.white,
            centerTitle: true,
            leading: IconButton(
              icon: const Icon(
                Icons.close_rounded,
                color: Colors.black
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ),
          SliverPadding(
            padding: const EdgeInsets.all(10),
            sliver: SliverToBoxAdapter(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text('Địa chỉ', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
                  Text(item!.address!, style: const TextStyle(fontSize: 16)),

                ],
              ),
            ),
          ),
          const SliverPadding(
            padding: EdgeInsets.all(10),
            sliver: SliverToBoxAdapter(
              child: Text(
                'Thời gian mở cửa',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)
              ),
            )
          ),
          SliverPadding(
            padding: const EdgeInsets.all(10),
            sliver: SliverList(
              delegate: SliverChildBuilderDelegate(
                    (BuildContext context, int index) {
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(item!.list![index].date!, style: TextStyle(fontSize: 16)),
                      Text(
                          '${item!.list![index].timeOpen} - ${item!.list![index].timeClose}',
                          style: const TextStyle(fontSize: 16)
                      ),
                    ],
                  );
                },
                childCount: 7, // 1000 list items
              ),
            ),
          )
        ],
      ),
    );;
  }
}
