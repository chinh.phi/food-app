import 'package:flutter/material.dart';
import 'package:food_app/app/modules/merchant/views/widgets/topping_menu_item.dart';
import 'package:food_app/re_use_function.dart';
import 'package:get/get.dart';

import '../controllers/topping_controller.dart';

class ToppingView extends GetView<ToppingController> {
  const ToppingView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.lazyPut(() => ToppingController());
    return Obx(() => Scaffold(
        appBar: _buildAppBar(),
        body: SingleChildScrollView(
            child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  _buildHeader(controller),
                  for(var i = 0; i < controller.itemLists.length; i++)
                    ToppingMenuItem(item: controller.itemLists[i], callbackFunction: controller.callback,)
                ]
            )
        ),
        bottomNavigationBar: _buildButtonAdd(controller)
    ));
  }

  _buildAppBar() {
    return AppBar(
      backgroundColor: Colors.white,
      title: const Text('Thêm món mới', style: TextStyle(color: Colors.black)),
      centerTitle: true,
      elevation: 0,
      actions: [
        IconButton(
          icon: const Icon(Icons.close, color: Colors.black),
          onPressed: () {
            controller.closeView();
            // Get.back();
          },
        ),
      ],
    );
  }

  _buildHeader(controller) {
    return Container(
      padding: EdgeInsets.all(10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: 90,
            height: 90,
            decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/sua_chua_tran_chau.png'),
                  fit: BoxFit.fill,
                )
            ),
          ),
          SizedBox(width: 10),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Sữa Chua Trân Châu', style: TextStyle(fontSize: 18)),
                Text('999+ đã bán | 100+ thích'),
                SizedBox(height: 25),
                Row(
                  children: [
                    Expanded(child: Text('${format(int.parse(controller.moneyInit))}', style: TextStyle(fontSize: 18, color: Colors.deepOrange))),
                    Container(
                      width: 30,
                      height: 30,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(color: Colors.deepOrange),
                      ),
                      child: IconButton(
                        padding: EdgeInsets.zero,
                        icon: const Icon(Icons.remove, color: Colors.deepOrange),
                        onPressed: () {
                          controller.clickRemoveFood();
                        },
                      ),
                    ),
                    SizedBox(width: 5),
                    Text('${controller.numberOfFood}', style: TextStyle(fontSize: 18)),
                    SizedBox(width: 5),
                    Container(
                      width: 30,
                      height: 30,
                      decoration: BoxDecoration(
                        color: Colors.red,
                      ),
                      child: IconButton(
                        padding: EdgeInsets.zero,
                        icon: const Icon(Icons.add, color: Colors.white),
                        onPressed: () {
                          controller.clickAddFood();
                        },
                      ),
                    )
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  _buildButtonAdd(controller) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: ElevatedButton(
        onPressed: () {
          controller.clickAddButton();
        },
        child: Text('Thêm vào giỏ hàng - ${format(int.parse(controller.money.value))}', style: const TextStyle(fontSize: 16),),
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all<Color>(Colors.deepOrange),
        ),
      ),
    );
  }
}
