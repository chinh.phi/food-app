import 'package:flutter/material.dart';
import 'package:food_app/app/modules/merchant/models/menu_food_model.dart';
import 'package:food_app/app/modules/merchant/views/widgets/food_item.dart';


class MenuItemOrder extends StatefulWidget {
  const MenuItemOrder({Key? key, this.menuLists, this.callbackFunction}) : super(key: key);
  final MenuFoodModel? menuLists;
  final Function? callbackFunction;
  @override
  State<MenuItemOrder> createState() => _MenuItemOrderState();
}

class _MenuItemOrderState extends State<MenuItemOrder> {

  var info;

  callback(result) {
    setState(() {
      info = result;
    });
    widget.callbackFunction!(info);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: double.infinity,
            padding: EdgeInsets.all(10),
            color: Colors.white,
            child: Text(
                '${widget.menuLists?.name})' ,
                style: const TextStyle(fontSize: 18, fontWeight: FontWeight.w500, color: Colors.black87)
            ),
          ),
          for(var i = 0; i < widget.menuLists!.lists!.length; i++)
            FoodItem(item: widget.menuLists!.lists![i], callbackFunction: callback,)
        ],
      ),
    );
  }
}
