import 'package:flutter/material.dart';
import 'package:food_app/app/modules/merchant/views/widgets/state_inherited_widget.dart';
import 'package:food_app/app/modules/merchant/views/widgets/topping_item.dart';

import '../../models/topping_model.dart';

class ToppingMenuItem extends StatefulWidget {
  const ToppingMenuItem({Key? key, required this.item, required this.callbackFunction}) : super(key: key);
  final ToppingModel item;
  final Function callbackFunction;
  @override
  State<ToppingMenuItem> createState() => _ToppingMenuItemState();
}

class _ToppingMenuItemState extends State<ToppingMenuItem> {

  String? toppingMoney;
  List moneyItemList = [];
  List item = [];
  int total = 0;
  List<int> numOfItemLists = [];
  bool isMax = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    moneyItemList = List.generate(widget.item.toppingLists!.length, (index) => '');
    numOfItemLists = List.generate(widget.item.toppingLists!.length, (index) => 0);
  }

  callback(money, id, num){
    setState(() {
      toppingMoney = '0';
      total = 0;
      moneyItemList[id] = money;
      numOfItemLists[id] = num;
      for (var e in moneyItemList) {
        if(e != '') {
          toppingMoney = (int.parse(toppingMoney!) + int.parse(e)).toString();
        }
      }
      for (var e in numOfItemLists) {
        total += e;
        if(total == widget.item.maxTopping) {
          isMax = true;
        } else {
          isMax = false;
        }
      }
      widget.callbackFunction(toppingMoney);
    });
  }

  @override
  Widget build(BuildContext context) {
    return StateInheritedWidget(
      isMax: isMax,
      child: Column(
        children: [
          Container(
              width: double.infinity,
              color: const Color(0xFFe8e8e8),
              padding: const EdgeInsets.all(10),
              child: Text('${widget.item.name} (Tối đa ${widget.item.maxTopping})', style: TextStyle(fontSize: 18))
          ),
          for(var i = 0 ; i < widget.item.toppingLists!.length ; i++)
            ToppingItem(id: i, item: widget.item.toppingLists![i],  callbackFunction: callback),
        ],
      ),
    );
  }
}
