import 'package:flutter/material.dart';
import 'package:food_app/app/modules/merchant/models/food_item_model.dart';
import 'package:food_app/app/modules/merchant/models/merchant_detail_model.dart';
import 'package:food_app/app/modules/merchant/views/topping_view.dart';
import 'package:food_app/re_use_function.dart';
import 'package:get/get.dart';


class FoodItem extends StatefulWidget {
  const FoodItem({Key? key, this.item, this.callbackFunction}) : super(key: key);
  final Food? item;
  final Function? callbackFunction;

  @override
  State<FoodItem> createState() => _FoodItemState();
}

class _FoodItemState extends State<FoodItem> {

  int numOfItem = 0;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border(
                bottom: BorderSide(
                    color: Color(0xFFe8e8e8)
                )
            )
        ),
        child: Row(
          children: [
            Expanded(
              child: Container(
                height: 90,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(widget.item!.name!, style: TextStyle(fontSize: 17, fontWeight: FontWeight.w500)),
                      Text(format(widget.item?.price), style: TextStyle(fontSize: 17, fontWeight: FontWeight.w500, color: Colors.red)),
                    ]
                ),
              ),
            ),

            Container(
                padding: EdgeInsets.only(left: 5),
                height: 110,
                width: 110,
                decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(widget.item!.imageLink!),
                      fit: BoxFit.fill,
                    )
                )
            ),
          ],
        ),
      ),
      onTap: () {
        Get.to(() => ToppingView(), arguments: [
          {"price": widget.item?.price},
        ])
            ?.then((result) {
          widget.callbackFunction!(result);
        });
      },
    );
  }
}
