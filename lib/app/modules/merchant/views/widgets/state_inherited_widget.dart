import 'package:flutter/material.dart';

class StateInheritedWidget extends InheritedWidget {
  final bool? isMax;

  const StateInheritedWidget({Key? key, this.isMax, required Widget child}) : super(key: key, child: child);

  static bool? of(BuildContext context) => context
      .dependOnInheritedWidgetOfExactType<StateInheritedWidget>()
      ?.isMax;

  @override
  bool updateShouldNotify(StateInheritedWidget oldWidget) => oldWidget.isMax != isMax;
  
}