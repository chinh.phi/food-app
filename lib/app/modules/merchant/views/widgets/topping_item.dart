import 'package:flutter/material.dart';
import 'package:food_app/app/modules/merchant/views/widgets/state_inherited_widget.dart';

import '../../models/topping_model.dart';

class ToppingItem extends StatefulWidget {
  const ToppingItem({Key? key, required this.id, required this.item, required this.callbackFunction}) : super(key: key);
  final ToppingItemModel item;
  final Function callbackFunction;
  final int id;
  @override
  State<ToppingItem> createState() => _ToppingItemState();
}

class _ToppingItemState extends State<ToppingItem> {
  int numOfItem = 0;
  String toppingMoney = '';
  bool isMax = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    isMax = StateInheritedWidget.of(context)!;
    var condition = isMax && numOfItem == 0;
    return Container(
        decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(
                    color: Color(0xFFe8e8e8),
                    width: 2
                )
            ),
            // color: Colors.white
            color: condition ? Color(0xFFf7f7f7) : Colors.white
        ),
        padding: EdgeInsets.all(10),
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('${widget.item.name}', style: TextStyle(fontSize: 18, fontWeight:FontWeight.w500, color: condition ? Colors.grey : Colors.black)),
                    Text('${widget.item.price}', style: TextStyle(fontSize: 16, color: condition ? Colors.grey : Colors.black))
                  ],
                ),
              ),
              toppingMoney != ''
                  ? Container(
                width: 30,
                height: 30,
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: Colors.deepOrange),
                ),
                child: IconButton(
                  padding: EdgeInsets.zero,
                  icon: const Icon(Icons.remove, color: Colors.deepOrange),
                  onPressed: () {
                    _clickRemove();

                  },
                ),
              )
                  : Container(),
              SizedBox(width: 5),
              numOfItem > 0 ? Text('${numOfItem}', style: TextStyle(fontSize: 18)) : Container(),
              SizedBox(width: 5),
              Container(
                width: 30,
                height: 30,
                decoration: BoxDecoration(
                  color: isMax ? Color(0xFFe8e8e8) : Colors.red,
                ),
                child: IconButton(
                  padding: EdgeInsets.zero,
                  icon: Icon(Icons.add, color: isMax ? Colors.black87 : Colors.white),
                  onPressed: isMax ? null : (){_clickAdd();},
                ),
              )
            ]
        )
    );
  }

  _clickAdd() {
    setState(() {
      numOfItem++;
      toppingMoney = (int.parse(widget.item.price!)*numOfItem).toString();
    });

    widget.callbackFunction(toppingMoney, widget.id, numOfItem);
  }

  _clickRemove(){
    setState(() {
      numOfItem--;
      if(numOfItem == 0) {
        toppingMoney = '';
      } else {
        toppingMoney = (int.parse(widget.item.price!)*numOfItem).toString();
      }
      widget.callbackFunction(toppingMoney, widget.id, numOfItem);
    });
  }


}
