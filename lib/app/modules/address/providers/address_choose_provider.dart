import 'dart:convert';

import 'package:food_app/user_model.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
class AddressChooseProvider {
  static var client = http.Client();
  static const _baseURL = "https://shopee-food-mobile.herokuapp.com";

  static Future<UserModel?> getUserInfo() async {
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString("token");

    var response = await client.get(Uri.parse('$_baseURL/user'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer $token'
        },
    );

    if(response.statusCode == 200) {
      UserModel data = UserModel.fromJson(jsonDecode(response.body));
      return data;
    } else {
      return null;
    }

  }
}
