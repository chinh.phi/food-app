import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

import 'package:http/http.dart' as http;

class OrderApi {

  static var client = http.Client();
  static const _baseURL = "https://shopee-food-mobile.herokuapp.com";

  static Future<List<dynamic>> getListOrder(endpoint) async {
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString("token");
    var response = await client.get(Uri.parse('$_baseURL$endpoint'),headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer $token'
    });
    if(response.statusCode == 200) {
      var json = response.body;
      return jsonDecode(json);
    } else {
      return [];
    }
  }

  static Future<dynamic> getOrderDetail(id) async {
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString("token");
    var response = await client.get(Uri.parse('$_baseURL/user/order/$id'),headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer $token'
    });
    if(response.statusCode == 200) {
      var json = response.body;
      return jsonDecode(json);
    } else {
      return '';
    }
  }
}