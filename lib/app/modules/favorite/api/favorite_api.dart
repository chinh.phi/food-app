import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

import 'package:http/http.dart' as http;

class FavoriteApi {
  static var client = http.Client();
  static const _baseURL = "https://shopee-food-mobile.herokuapp.com";

  static Future<List<dynamic>> getFavorite() async {
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString("token");
    var response = await client.get(Uri.parse('$_baseURL/user/favourite'),headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer $token'
    });
    if(response.statusCode == 200) {
      var json = response.body;
      return jsonDecode(json);
    } else {
      return [];
    }
  }

  static Future<String> deleteFavorite(id) async {
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString("token");
    var response = await client.delete(Uri.parse('$_baseURL/user/favourite'),headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer $token'
    },
      body: jsonEncode({"idRes": id}));
    if(response.statusCode == 200) {
      var json = response.body;
      return json;
    } else {
      return 'Lỗi';
    }
  }
}
