
import 'dart:convert';
import 'package:http/http.dart' as http;

import '../models/error_resp.dart';
import '../models/login_resp.dart';

class LoginApi {
  static var client = http.Client();
  static const _baseURL = "https://shopee-food-mobile.herokuapp.com";

  static Future<List> login({String? username, String? password}) async {
    var response = await client.post(Uri.parse('$_baseURL/auth/login'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body:
        jsonEncode(<String, String>{"username": username ?? '', "password": password ?? ''}));

    if(response.statusCode == 200) {
      var json = response.body;
      var loginRes = loginRespFromJson(json);
      print(loginRes.accesstoken);
      if (loginRes != null) {
        return [loginRes.accesstoken, loginRes.refreshtoken];
      } else {
        return ["", "Unknown Error"];
      }
    } else {
      var json = response.body;
      var errorResp = errorRespFromJson(json);
      print(errorResp);
      if (errorResp == null) {
        return ["", "Unknown Error"];
      } else {
        return ["", errorResp.error!.message];
      }
    }
  }

  static Future<List> refreshToken({String? token}) async {
    var response =
    await client.post(Uri.parse('$_baseURL/auth/refresh-token'));

    if (response.statusCode == 200) {
      var json = response.body;
      //status is success but not excepted result
      if (json.contains("accesstoken") == false) {
        return ["", "Unknown Error"];
      }
      var loginRes = loginRespFromJson(json);
      if (loginRes != null) {
        return [loginRes.accesstoken, ""];
      } else {
        return ["", "Unknown Error"];
      }
    } else {
      var json = response.body;
      var errorResp = errorRespFromJson(json);
      if (errorResp == null) {
        return ["", "Unknown Error"];
      } else {
        return ["", errorResp.error!.message];
      }
    }
  }

  static Future<bool> logout({String? token}) async {
    var response =
    await client.post(Uri.parse('$_baseURL/auth/logout'), headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
        body:
        jsonEncode({"refreshtoken": token}));
    if(response.statusCode == 204) {
      return true;
    } else {
      return false;

    }
  }
}
