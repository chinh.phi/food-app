import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../api/login_api.dart';

class LoginController extends GetxController {
  var loginProcess = false.obs;
  var error = "";

  Future<String> login({String? username, String? password}) async {
    error = "";
    try {
      loginProcess(true);
      List loginResp = await LoginApi.login(username: username, password: password);
      if (loginResp[0] != "") {
        //success
        final prefs = await SharedPreferences.getInstance();
        prefs.setString("token", loginResp[0]);
        prefs.setString('refreshToken', loginResp[1]);
      } else {
        error = loginResp[1];
      }
    } finally {
      loginProcess(false);
    }
    return error;
  }

  Future<bool> refresh() async {
    final prefs = await SharedPreferences.getInstance();
    String? token = prefs.getString("token");

    if (token == null || token == "") {
      return false;
    }

    bool success = false;
    try {
      loginProcess(true);
      List loginResp = await LoginApi.refreshToken(token: token);
      if (loginResp[0] != "") {
        //success
        final prefs = await SharedPreferences.getInstance();
        prefs.setString("token", loginResp[0]);
        success = true;
      }
    } finally {
      loginProcess(false);
    }
    return success;
  }

  Future<bool> logout() async {
    final prefs = await SharedPreferences.getInstance();
    String? token = prefs.getString("refreshToken");
    bool logoutResp = await LoginApi.logout(token: token!);
    return logoutResp;
  }
}