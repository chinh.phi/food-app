
import 'dart:convert';

import 'package:food_app/app/modules/home/models/home_menu_model.dart';
import 'package:food_app/app/modules/search/models/history_search_model.dart';
import 'package:food_app/app/modules/search/models/search_result_.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:http/http.dart' as http;

class SearchApi {
  static var client = http.Client();
  static const _baseURL = "https://shopee-food-mobile.herokuapp.com";

  static Future<List<dynamic>> getHistorySearch() async {
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString("token");
    var response = await client.get(Uri.parse('$_baseURL/user/search/history?limit=10'),headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer $token'
    });
    if(response.statusCode == 200) {
      var json = response.body;
      return jsonDecode(json);
    } else {
      return [];
    }
  }

  static Future<dynamic> deleteHistoryById(id) async {
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString("token");

    var response = await client.delete(Uri.parse('$_baseURL/user/search/history/${id}'),headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer $token'
    });

    if(response.statusCode == 200) {
      return 'Success';
    } else {
      return '';
    }
  }

  static Future<dynamic> deleteAllHistory() async {
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString("token");

    var response = await client.delete(Uri.parse('$_baseURL/user/search/history'),headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Authorization': 'Bearer $token'
    });

    if(response.statusCode == 200) {
      return 'Success';
    } else {
      return '';
    }
  }

  static Future<List<dynamic>> getPopularFood() async {
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString("token");

    var response = await client.get(Uri.parse('$_baseURL/restaurant/category/popular?limit=8'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer $token'
        });

    if(response.statusCode == 200) {
      var json = response.body;
      return jsonDecode(json);
    } else {
      return [];
    }
  }

  static Future<List<SearchResultModel>> searchFood(searchValue, pageNumber, pageSize) async {
    final prefs = await SharedPreferences.getInstance();
    final token = prefs.getString("token");

    var response = await client.post(Uri.parse('$_baseURL/homepage/search'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer $token'
        },
        body: jsonEncode({"searchValue": searchValue, "pageNumber": pageNumber, "pageSize": pageSize, "isSearch": true}));

    if(response.statusCode == 200) {
      var json = response.body;
      List<SearchResultModel> tmp = List<SearchResultModel>.from(jsonDecode(json).map((model)=> SearchResultModel.fromJson(model)));
      return tmp;
    } else {
      return [];
    }
  }
}