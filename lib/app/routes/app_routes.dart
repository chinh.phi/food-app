part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  static const SPLASHSCREEN = _Paths.SPLASHSCREEN;
  static const MERCHANTDETAIL = _Paths.MERCHANTDETAIL;
  static const ORDERDETAIL = _Paths.ORDERDETAIL;
}

abstract class _Paths {
  static const SPLASHSCREEN = '/splashscreen';
  static const MERCHANTDETAIL = '/merchant-detail/:id';
  static const ORDERDETAIL = '/order-detail/:id';
}