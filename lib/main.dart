import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:food_app/app/routes/app_pages.dart';
import 'package:food_app/dimensions.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';



void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Application",
      theme: ThemeData(
        // Define the default brightness and colors.
        primaryColor: Dimensions.primaryColor,

        // Define the default font family.
        fontFamily: 'Joan',
      ),
      initialRoute: AppPages.INITIAL,
      getPages: AppPages.routes,
    );
  }
}
